<?php

namespace App\Commands;

use Orchestra\Maestro\Contracts\Command;
use Orchestra\Maestro\Traits\Command as TraitsCommand;

class TestCommand implements Command
{
   use TraitsCommand;

   public function run()
   {
      dump("TEST");
   }
}

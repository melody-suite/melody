<?php

function cliCommands()
{
   return [
      "make:command" => Orchestra\Maestro\Commands\MakeCommand::class,
      "test"         => App\Commands\TestCommand::class
   ];
}

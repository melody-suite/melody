<?php

namespace App\Domains\Example;

use App\Domains\Example\Actions\ExampleAction;
use Orchestra\Facades\Route;

class Routes
{
   public static function routes()
   {
      Route::group(['prefix' => 'admin'], function ($route) {

         $route->get('example', ExampleAction::class);
      });
   }
}

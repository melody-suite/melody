<?php

namespace App\Domains\Example\Actions;

use Orchestra\Action\Contracts\AsController;
use Orchestra\Action\Traits\AsController as TraitsAsController;

class ExampleAction implements AsController
{
   use TraitsAsController;

   public function validate()
   {
      return [
         "name" => ["nullable"]
      ];
   }

   public function handle()
   {
      return "This is an example Action";
   }
}

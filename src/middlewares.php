<?php

use Orchestra\Middlewares\ResolveRoute;
use Orchestra\Middlewares\ReturnPlainResponse;

function requestMiddlewares()
{
   return [
      ResolveRoute::class,
   ];
}

function responseMiddlewares()
{
   return [
      ReturnPlainResponse::class,
   ];
}

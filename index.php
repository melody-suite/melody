<?php

use Tracy\Debugger;

require_once __DIR__ . '/vendor/autoload.php';

require_once __DIR__ . '/src/middlewares.php';

require_once __DIR__ . '/src/routes.php';

require_once __DIR__ . '/src/aliases.php';

$isDev = env("APP_MODE") == "dev";

Debugger::$strictMode = $isDev;

Debugger::enable($isDev ? Debugger::DEVELOPMENT : Debugger::PRODUCTION);

createAliases();

run();

function createAliases()
{
   foreach (aliases() as $alias => $class) {
      class_alias($class, $alias);
   }
}
